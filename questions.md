1. What is the expected TPS of the REST API?
-- Used Spring Webflux (Pure Non-Blocking) framework support to maximize the performance
-- Used MongoDB for horizontally scalability on demand
2. Issues in the existing calculation logic?
-- while loop operate only startTime < endTime. But need to be correct to startTime <= endTime
-- Handled IsWeekEnds? , Is Monthly or daily fees?, isExemptVehicle scenarios
3. What are the dynamic data and How can it externalize the dynamic data?
-- Public holidays, hourly rate for each city monthly rates for each city,exempt vehicles can be varying and need to be changed in runtime without effecting the code. 
-- Can use MongoDb collections for each scenario.
4. What would be the API signature and what are the request input parameters?
-- date, startDate and endDate can be user as Path variables
-- vehicleType, daily or monthly subscription, parking city can be used together in the request body


