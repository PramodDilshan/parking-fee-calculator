package com.example.parkingfeecalculator.service;
import com.example.parkingfeecalculator.dto.Vehicle;
import com.example.parkingfeecalculator.dto.VehicleRequest;
import com.example.parkingfeecalculator.entity.HourlyFee;
import com.example.parkingfeecalculator.entity.MonthlyFee;
import com.example.parkingfeecalculator.repository.ExemptVehicleRepository;
import com.example.parkingfeecalculator.repository.HourlyFeeRepository;
import com.example.parkingfeecalculator.repository.MonthlyFeeRepository;
import com.example.parkingfeecalculator.repository.PublicHolidayRepository;
import com.example.parkingfeecalculator.utils.CalendarHelper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.LocalDate;
import java.time.LocalTime;

@ExtendWith(MockitoExtension.class)
class CalculatorServiceTest {

    @Mock
    private MonthlyFeeRepository monthlyFeeRepository;

    @Mock
    private PublicHolidayRepository publicHolidayRepository;

    @Mock
    private HourlyFeeRepository hourlyFeeRepository;

    @Mock
    private ExemptVehicleRepository exemptVehicleRepository;

    @Mock
    private CalendarHelper calendarHelper;

    @InjectMocks
    private CalculatorService calculatorService;





    @Test
    void testCalculateParkingFee_DailySubscription() {
        VehicleRequest vehicle = new VehicleRequest("VAN", "DAILY", "COLOMBO");
        LocalDate date = LocalDate.of(2024, 5, 7);
        LocalTime startTime = LocalTime.of(0, 0);
        LocalTime endTime = LocalTime.of(1, 0);
        double expectedFee = 150.0;

        Mockito.when(exemptVehicleRepository.existsExemptVehicleByType(Mockito.any(String.class))).thenReturn(Mono.just(false));
        Mockito.when(publicHolidayRepository.existsPublicHolidayByDate(Mockito.any())).thenReturn(Mono.just(false));
        HourlyFee hourlyFee0 = new HourlyFee();
        hourlyFee0.setFee(100.00);
        hourlyFee0.setId("X00");
        hourlyFee0.setHour(0);
        hourlyFee0.setCity("COLOMBO");
        HourlyFee hourlyFee1 = new HourlyFee();
        hourlyFee1.setFee(50.00);
        hourlyFee1.setId("X01");
        hourlyFee1.setHour(1);
        hourlyFee0.setCity("COLOMBO");
        Mockito.when(hourlyFeeRepository.findHourlyFeeByHourAndCity(0, "COLOMBO")).thenReturn(Mono.just(hourlyFee0));
        Mockito.when(hourlyFeeRepository.findHourlyFeeByHourAndCity(1, "COLOMBO")).thenReturn(Mono.just(hourlyFee1));
        Mockito.when(calendarHelper.isWeekEnd(LocalDate.of(2024,5,7))).thenReturn(Mono.just(false));

        StepVerifier.create(calculatorService.calculateParkingFee(vehicle, date, startTime, endTime))
                .expectNext(expectedFee)
                .verifyComplete();
    }

    @Test
    void testIsFeeExemptVehicle_True() {
        Vehicle vehicle = new VehicleRequest("EMERGENCY", "DAILY", "COLOMBO");

        Mockito.when(exemptVehicleRepository.existsExemptVehicleByType(Mockito.any(String.class))).thenReturn(Mono.just(true));

        StepVerifier.create(calculatorService.isFeeExemptVehicle(vehicle))
                .expectNext(true)
                .verifyComplete();
    }

    @Test
    void testIsFeeExemptVehicle_False() {
        Vehicle vehicle = new VehicleRequest("CAR", "DAILY", "COLOMBO");

        Mockito.when(exemptVehicleRepository.existsExemptVehicleByType(Mockito.any(String.class))).thenReturn(Mono.just(false));

        StepVerifier.create(calculatorService.isFeeExemptVehicle(vehicle))
                .expectNext(false)
                .verifyComplete();
    }

    @Test
    void testGetMonthlyFee() {
        VehicleRequest vehicle = new VehicleRequest("CAR", "DAILY", "COLOMBO");

        MonthlyFee monthlyFee = new MonthlyFee();
        monthlyFee.setFee(5000.0);

        Mockito.when(monthlyFeeRepository.findMonthlyFeeByVehicleTypeAndCity(Mockito.any(String.class), Mockito.any(String.class))).thenReturn(Mono.just(monthlyFee));

        StepVerifier.create(calculatorService.getMonthlyFee(vehicle))
                .expectNext(5000.00)
                .verifyComplete();
    }

    @Test
    void calculateParkingFee_ExemptVehicle_ReturnsZero() {
        VehicleRequest vehicle = new VehicleRequest("Car", "DAILY", "City");
        LocalDate date = LocalDate.now();
        LocalTime startTime = LocalTime.now();
        LocalTime endTime = startTime.plusHours(1);

        Mockito.when(exemptVehicleRepository.existsExemptVehicleByType(Mockito.anyString())).thenReturn(Mono.just(true));

        StepVerifier.create(calculatorService.calculateParkingFee(vehicle, date, startTime, endTime))
                .expectNext(0.0)
                .verifyComplete();
    }

    @Test
    void calculateParkingFee_Weekend_ReturnsZero() {
        VehicleRequest vehicle = new VehicleRequest("Car", "DAILY", "City");
        LocalDate date = LocalDate.of(2024, 5, 11); // Saturday
        LocalTime startTime = LocalTime.now();
        LocalTime endTime = startTime.plusHours(1);

        Mockito.when(exemptVehicleRepository.existsExemptVehicleByType(Mockito.anyString())).thenReturn(Mono.just(false));
        Mockito.when(calendarHelper.isWeekEnd(LocalDate.of(2024,5,11))).thenReturn(Mono.just(true));

        StepVerifier.create(calculatorService.calculateParkingFee(vehicle, date, startTime, endTime))
                .expectNext(0.0)
                .verifyComplete();
    }

    @Test
    void calculateParkingFee_PublicHoliday_ReturnsZero() {
        VehicleRequest vehicle = new VehicleRequest("Car", "DAILY", "City");
        LocalDate date = LocalDate.of(2024, 1, 1); // New Year's Day
        LocalTime startTime = LocalTime.now();
        LocalTime endTime = startTime.plusHours(1);

        Mockito.when(exemptVehicleRepository.existsExemptVehicleByType(Mockito.anyString())).thenReturn(Mono.just(false));
        Mockito.when(calendarHelper.isWeekEnd(LocalDate.of(2024,1,1))).thenReturn(Mono.just(false));
        Mockito.when(publicHolidayRepository.existsPublicHolidayByDate(Mockito.any())).thenReturn(Mono.just(true));

        StepVerifier.create(calculatorService.calculateParkingFee(vehicle, date, startTime, endTime))
                .expectNext(0.0)
                .verifyComplete();
    }





}

