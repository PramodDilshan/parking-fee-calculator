package com.example.parkingfeecalculator.controller;

import com.example.parkingfeecalculator.dto.VehicleRequest;
import com.example.parkingfeecalculator.service.CalculatorService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.time.LocalTime;

@ExtendWith(MockitoExtension.class)
class CalculatorControllerTest {

    @Mock
    private CalculatorService calculatorService;

    @InjectMocks
    private CalculatorController calculatorController;

    @Test
    void testCalculateParkingFee() {
        VehicleRequest vehicleRequest = new VehicleRequest(); // Instantiate with required data
        LocalDate date = LocalDate.of(2024, 5, 7);
        LocalTime startTime = LocalTime.of(10, 0);
        LocalTime endTime = LocalTime.of(12, 0);
        double expectedFee = 10.0; // Set expected fee

        Mockito.when(calculatorService.calculateParkingFee(Mockito.any(VehicleRequest.class), Mockito.any(LocalDate.class), Mockito.any(LocalTime.class),Mockito.any(LocalTime.class)))
                .thenReturn(Mono.just(expectedFee));
        // Perform the HTTP POST request and verify the response
        WebTestClient.bindToController(calculatorController).build()
                .post()
                .uri("/calculator?date={date}&startTime={startTime}&endTime={endTime}", date, startTime, endTime)
                .bodyValue(vehicleRequest)
                .exchange()
                .expectStatus().isOk()
                .expectBody(Double.class)
                .isEqualTo(expectedFee);
    }
}
