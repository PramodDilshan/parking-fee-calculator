package com.example.parkingfeecalculator;

import com.example.parkingfeecalculator.controller.CalculatorController;
import com.example.parkingfeecalculator.service.CalculatorService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ParkingFeeCalculatorApplicationTests {
    @Autowired
    private CalculatorService calculatorService;

    @Autowired
    private CalculatorController calculatorController;

    @Test
    void contextLoads() {
        Assertions.assertNotNull(calculatorService);
        Assertions.assertNotNull(calculatorController);
    }

}
