package com.example.parkingfeecalculator.service;
import com.example.parkingfeecalculator.dto.VehicleRequest;
import com.example.parkingfeecalculator.dto.Vehicle;
import com.example.parkingfeecalculator.entity.HourlyFee;
import com.example.parkingfeecalculator.repository.ExemptVehicleRepository;
import com.example.parkingfeecalculator.repository.HourlyFeeRepository;
import com.example.parkingfeecalculator.repository.MonthlyFeeRepository;
import com.example.parkingfeecalculator.repository.PublicHolidayRepository;
import com.example.parkingfeecalculator.utils.CalendarHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.Date;

@Service
public class CalculatorService {


    private final MonthlyFeeRepository monthlyFeeRepository;
    private final PublicHolidayRepository publicHolidayRepository;
    private final HourlyFeeRepository hourlyFeeRepository;
    private final ExemptVehicleRepository exemptVehicleRepository;

    private final CalendarHelper calendarHelper;
    @Autowired
    public CalculatorService(MonthlyFeeRepository monthlyFeeRepository,
                             PublicHolidayRepository publicHolidayRepository,
                             HourlyFeeRepository hourlyFeeRepository,
                             ExemptVehicleRepository exemptVehicleRepository, CalendarHelper calendarHelper) {
        this.monthlyFeeRepository = monthlyFeeRepository;
        this.publicHolidayRepository = publicHolidayRepository;
        this.hourlyFeeRepository = hourlyFeeRepository;
        this.exemptVehicleRepository = exemptVehicleRepository;
        this.calendarHelper = calendarHelper;
    }

    /**
     * Calculates the parking fee for a given vehicle request based on the specified date, start time, and end time.
     * The calculation takes into account factors such as exemption status, weekday/weekend, public holiday,
     * and subscription type.
     *
     * @param vehicle   The {@link VehicleRequest} representing the vehicle for which the parking fee is being calculated.
     * @param date      The date of the parking session.
     * @param startTime The start time of the parking session.
     * @param endTime   The end time of the parking session.
     * @return A {@link reactor.core.publisher.Mono} emitting a {@link Double} value representing the calculated parking fee.
     */
    public Mono<Double> calculateParkingFee(VehicleRequest vehicle, LocalDate date, LocalTime startTime, LocalTime endTime) {
        return isFeeExemptVehicle(vehicle)
                .flatMap(isExempt -> handleExemptVehicle(isExempt, vehicle, date, startTime, endTime))
                .defaultIfEmpty(0.0);
    }

    /**
     * Handles the case when the vehicle is exempt from parking fees.
     *
     * @param isExempt   A {@link Boolean} indicating whether the vehicle is exempt from parking fees.
     * @param vehicle    The {@link VehicleRequest} representing the vehicle.
     * @param date       The date of the parking session.
     * @param startTime  The start time of the parking session.
     * @param endTime    The end time of the parking session.
     * @return A {@link reactor.core.publisher.Mono} emitting a {@link Double} value representing the calculated parking fee.
     */
    private Mono<Double> handleExemptVehicle(Boolean isExempt, VehicleRequest vehicle, LocalDate date, LocalTime startTime, LocalTime endTime) {
        if (Boolean.TRUE.equals(isExempt)) {
            return Mono.just(0.0);
        }
        return calendarHelper.isWeekEnd(date)
                .flatMap(isWeekend -> handleWeekend(isWeekend, vehicle, date, startTime, endTime));
    }

    /**
     * Handles the case when the parking session occurs on a weekend.
     *
     * @param isWeekend  A {@link Boolean} indicating whether the parking session occurs on a weekend.
     * @param vehicle    The {@link VehicleRequest} representing the vehicle.
     * @param date       The date of the parking session.
     * @param startTime  The start time of the parking session.
     * @param endTime    The end time of the parking session.
     * @return A {@link reactor.core.publisher.Mono} emitting a {@link Double} value representing the calculated parking fee.
     */
    private Mono<Double> handleWeekend(Boolean isWeekend, VehicleRequest vehicle, LocalDate date, LocalTime startTime, LocalTime endTime) {
        if (Boolean.TRUE.equals(isWeekend)) {
            return Mono.just(0.0);
        }
        return isPublicHoliday(date)
                .flatMap(isPublicHoliday -> handlePublicHoliday(isPublicHoliday, vehicle, startTime, endTime));
    }

    /**
     * Handles the case when the parking session occurs on a public holiday.
     *
     * @param isPublicHoliday  A {@link Boolean} indicating whether the parking session occurs on a public holiday.
     * @param vehicle          The {@link VehicleRequest} representing the vehicle.
     * @param startTime        The start time of the parking session.
     * @param endTime          The end time of the parking session.
     * @return A {@link reactor.core.publisher.Mono} emitting a {@link Double} value representing the calculated parking fee.
     */
    private Mono<Double> handlePublicHoliday(Boolean isPublicHoliday, VehicleRequest vehicle, LocalTime startTime, LocalTime endTime) {
        if (Boolean.TRUE.equals(isPublicHoliday)) {
            return Mono.just(0.0);
        }
        return calculateFeeBasedOnSubscription(vehicle, startTime, endTime);
    }

    /**
     * Calculates the parking fee based on the subscription type of the vehicle.
     *
     * @param vehicle    The {@link VehicleRequest} representing the vehicle.
     * @param startTime  The start time of the parking session.
     * @param endTime    The end time of the parking session.
     * @return A {@link reactor.core.publisher.Mono} emitting a {@link Double} value representing the calculated parking fee.
     */

    private Mono<Double> calculateFeeBasedOnSubscription(VehicleRequest vehicle, LocalTime startTime, LocalTime endTime) {
        if ("DAILY".equalsIgnoreCase(vehicle.getSubscription())) {
            return calculateHourlyFee(startTime, endTime, vehicle.getCity());
        } else {
            return getMonthlyFee(vehicle);
        }
    }

    /**
     * Calculates the hourly parking fee recursively for the specified time interval and city.
     *
     * @param startTime The start time of the parking session.
     * @param endTime   The end time of the parking session.
     * @param city      The city where the parking session occurs.
     * @return A {@link reactor.core.publisher.Mono} emitting a {@link Double} value representing the calculated hourly parking fee.
     */
    public Mono<Double> calculateHourlyFee(LocalTime startTime, LocalTime endTime, String city) {
        return calculateHourlyFeeRecursive(startTime, endTime, 0.0, city);
    }

    /**
     * Helper method to calculate the hourly parking fee recursively.
     *
     * @param startTime The start time of the current hour.
     * @param endTime   The end time of the parking session.
     * @param totalFee  The total fee accumulated so far during the recursive calculation.
     * @param city      The city where the parking session occurs.
     * @return A {@link reactor.core.publisher.Mono} emitting a {@link Double} value representing the calculated hourly parking fee.
     */
    private Mono<Double> calculateHourlyFeeRecursive(LocalTime startTime, LocalTime endTime, double totalFee, String city) {
        if (startTime.isAfter(endTime)) {
            return Mono.just(totalFee);
        } else {
            return hourlyFeeRepository.findHourlyFeeByHourAndCity(startTime.getHour(), city).map(HourlyFee::getFee)
                    .flatMap(fee -> calculateHourlyFeeRecursive(startTime.plusHours(1), endTime, totalFee + fee, city));
        }
    }

    /**
     * Checks if a vehicle is exempt from parking fees based on its type.
     *
     * @param vehicle The {@link Vehicle} object representing the vehicle to check for exemption.
     * @return A {@link reactor.core.publisher.Mono} emitting a boolean value indicating whether the specified vehicle type is exempt from parking fees.
     */
    public Mono<Boolean> isFeeExemptVehicle(Vehicle vehicle) {
        return exemptVehicleRepository.existsExemptVehicleByType(vehicle.getVehicleType());
    }

    /**
     * Retrieves the monthly fee for a vehicle based on its type and city.
     *
     * @param vehicle The {@link VehicleRequest} containing information about the vehicle, including its type and city.
     * @return A {@link reactor.core.publisher.Mono} emitting a {@link Double} value representing the monthly fee for the specified vehicle type and city.
     */
    public Mono<Double> getMonthlyFee(VehicleRequest vehicle){
        return monthlyFeeRepository.findMonthlyFeeByVehicleTypeAndCity(vehicle.getVehicleType(), vehicle.getCity()).flatMap(
                monthlyFee -> Mono.just(monthlyFee.getFee())
        );
    }

    /**
     * Checks if a given date is a public holiday.
     *
     * @param date The date to check if it's a public holiday.
     * @return A {@link reactor.core.publisher.Mono} emitting a boolean value indicating whether the given date is a public holiday.
     */
    public Mono<Boolean> isPublicHoliday(LocalDate date) {
        return publicHolidayRepository.existsPublicHolidayByDate(Date.from(date.atStartOfDay(ZoneOffset.UTC).toInstant()));
    }




}