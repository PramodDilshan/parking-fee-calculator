package com.example.parkingfeecalculator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ParkingFeeCalculatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(ParkingFeeCalculatorApplication.class, args);
    }

}
