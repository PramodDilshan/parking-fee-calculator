package com.example.parkingfeecalculator.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VehicleRequest implements Vehicle, Parking{
    private String vehicleType;
    private String subscription;
    private String city;
}
