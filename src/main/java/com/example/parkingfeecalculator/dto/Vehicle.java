package com.example.parkingfeecalculator.dto;

public interface Vehicle {
    String getVehicleType();
}