package com.example.parkingfeecalculator.repository;

import com.example.parkingfeecalculator.entity.ExemptVehicle;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface ExemptVehicleRepository extends ReactiveMongoRepository<ExemptVehicle, String> {

    Mono<Boolean> existsExemptVehicleByType(@Param("type") String type);
}
