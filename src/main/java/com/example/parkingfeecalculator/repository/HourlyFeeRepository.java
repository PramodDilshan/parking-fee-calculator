package com.example.parkingfeecalculator.repository;

import com.example.parkingfeecalculator.entity.HourlyFee;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface HourlyFeeRepository extends ReactiveMongoRepository<HourlyFee, String> {

    Mono<HourlyFee> findHourlyFeeByHourAndCity(@Param("hour") int hour, @Param("city") String city);
}
