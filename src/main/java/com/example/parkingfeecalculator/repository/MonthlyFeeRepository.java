package com.example.parkingfeecalculator.repository;

import com.example.parkingfeecalculator.entity.MonthlyFee;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface MonthlyFeeRepository extends ReactiveMongoRepository<MonthlyFee, String> {

    Mono<MonthlyFee> findMonthlyFeeByVehicleTypeAndCity(@Param("vehicleType") String vehicleType, @Param("city") String city);

}
