package com.example.parkingfeecalculator.repository;

import com.example.parkingfeecalculator.entity.PublicHoliday;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.util.Date;
@Repository
public interface PublicHolidayRepository extends ReactiveMongoRepository<PublicHoliday, String> {

    Mono<Boolean> existsPublicHolidayByDate(@Param("date") Date date);

}
