package com.example.parkingfeecalculator.utils;

import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.time.DayOfWeek;
import java.time.LocalDate;

@Component
public class CalendarHelper {

    public Mono<Boolean> isWeekEnd(LocalDate date) {
        return Mono.just(date.getDayOfWeek() == DayOfWeek.SATURDAY || date.getDayOfWeek() == DayOfWeek.SUNDAY);
    }
}
