package com.example.parkingfeecalculator.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "exempt-vehicle")
public class ExemptVehicle {

    @Id
    private String id;

    private String type;
}
