package com.example.parkingfeecalculator.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "hourly-fee")
public class HourlyFee {

    @Id
    String id;

    private int hour;

    private double fee;

    private String city;
}
