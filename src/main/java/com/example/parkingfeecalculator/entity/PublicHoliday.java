package com.example.parkingfeecalculator.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Document(collection = "public-holiday")
public class PublicHoliday {
    @Id
    private String id;

    private Date date;
}
