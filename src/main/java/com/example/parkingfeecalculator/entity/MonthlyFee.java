package com.example.parkingfeecalculator.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "monthly-fee")
public class MonthlyFee {

    @Id
    private String id;

    private String vehicleType;

    private double fee;

    private String city;
}
