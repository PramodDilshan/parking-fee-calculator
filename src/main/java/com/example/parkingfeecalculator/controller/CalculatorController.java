package com.example.parkingfeecalculator.controller;
import com.example.parkingfeecalculator.dto.VehicleRequest;
import com.example.parkingfeecalculator.service.CalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.time.LocalTime;

@RestController
@RequestMapping("/calculator")
public class CalculatorController {


    private final CalculatorService calculatorService;
    @Autowired
    public CalculatorController(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;
    }

    /**
     * Calculates the parking fee for a given vehicle request based on the specified date, start time, and end time.
     * The calculation takes into account factors such as exemption status, weekday/weekend, public holiday,
     * and subscription type.
     *
     * @param vehicleRequest The {@link VehicleRequest} representing the vehicle and its attributes.
     * @param date           The date of the parking session.
     * @param startTime      The start time of the parking session.
     * @param endTime        The end time of the parking session.
     * @return A {@link reactor.core.publisher.Mono} emitting a {@link Double} value representing the calculated parking fee.
     */
    @PostMapping
    public Mono<Double> calculateParkingFee(@RequestBody VehicleRequest vehicleRequest,
                                            @RequestParam("date") LocalDate date,
                                            @RequestParam("startTime")LocalTime startTime,
                                            @RequestParam("endTime")LocalTime endTime) {
        return calculatorService.calculateParkingFee(vehicleRequest, date, startTime, endTime);
    }
}
