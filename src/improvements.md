1. Loggers should be added for each method
2. Write unite test to increase the coverage for negative scenarios
3. Currently, there is no APIs for updating database stored data. CRUD APIs should be implemented for each collection.
4. According to the content editors web portal, backend APIs should be designed.
5. Load test should be executed for benchmark the performance
6. Sonar issues should be fixed (calculateParkingFee method's logic complexity issue)
7. Introduce enums for subscription
8. Interface should be introduced ad cde should be implemented according ot the interfaces. 
9. Design patterns may be applied and refactored the codebase to generalize the solution and accommodate feature requirements without breaking the code. 